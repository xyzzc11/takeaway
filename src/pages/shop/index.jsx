import React, { useEffect, useState } from 'react'
import Panner from '../../components/Panner'
import { Button, Form, Input, InputNumber, TimePicker, Checkbox, Upload } from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import { shopInfo, shopUpload } from '../../api/shop'
import dayjs from 'dayjs';
const { TextArea } = Input;
export default function Shop() {
  // 活动名称数组
  const active = ['单人精彩套餐', 'VC无限橙果汁全场8折', '在线支付满28减五', '特价饮品八折抢购', '中秋特惠', '国庆特价', '春节1折折扣']
  const [shopAvatar, setShopAvatar] = useState()
  const [shopPics, setShopPics] = useState([])
  const [form] = Form.useForm()


  // 获取店铺详情
  const shop = async () => {
    const res = await shopInfo()
    console.log(res);
    const { name, bulletin, avatar, pics, minPrice, deliveryTime, description, score, sellCount, supports, date } = res.data.data
    setShopAvatar(avatar)
    // setShopPics(pics)

    form.setFieldsValue({
      name, bulletin,
      //  avatar, pics, 
      minPrice,
      deliveryTime,
      description, score, sellCount, supports,
      date: [dayjs(date[0]), dayjs(date[1])]
    })
  }

  const uploadShopImg = async (e) => {
    // console.log(e);
    const formData = new FormData()
    formData.append('file', e.file)
    const res = await shopUpload(formData)
    console.log(res);
    setAvatar(res.data.imgUrl)
  }



  const [fileList, setFileList] = useState([
    {
      uid: '-1',
      name: 'image.png',
      status: 'done',
      url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
    },
  ]);
  const onChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };
  const onPreview = async (file) => {
    let src = file.url;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };







  useEffect(() => {
    shop()
  }, [])

  return (
    <>
      <Panner>
        <div slot='title'>店铺管理</div>
        <Button type='primary' slot='btn' style={{ height: 40 }}>保存店铺信息</Button>

        <Form
          labelCol={{
            span: 2,
          }}
          style={{ marginTop: 20, marginRight: 10 }}
          form={form}
        >
          <Form.Item label='店铺名称' name='name'>
            <Input style={{ height: 40 }}></Input>
          </Form.Item>
          <Form.Item label='店铺公告' name='bulletin'>
            <TextArea></TextArea>
          </Form.Item>
          <Form.Item label='店铺头像' name='avatar'>
            <Upload
              customRequest={uploadShopImg}
              listType='picture-card'
              showUploadList={false} maxCount={1}
            >
              {<img src={'http://8.137.157.16:9002' + shopAvatar} style={{ top: 0, left: 0, width: 100, height: 100, overflow: 'hidden' }} />}

            </Upload>
          </Form.Item>
          <Form.Item label='店铺图片' name='pics'>

            {shopPics.map((item, index) =>
              <img src={'http://8.137.157.16:9002' + item} key={index} style={{ width: 100, height: 100, marginRight: 20 }} />
            )}
          </Form.Item>
          <Form.Item label='起送价格' name='minPrice'>
            <Input style={{ height: 40 }}></Input>
          </Form.Item>
          <Form.Item label='送达时间' name='deliveryTime'>
            <Input style={{ height: 40 }}></Input>
          </Form.Item>
          <Form.Item label='配送描述' name='description'>
            <Input style={{ height: 40 }}></Input>
          </Form.Item>
          <Form.Item label='店铺好评率' name='score'>
            <InputNumber></InputNumber>
          </Form.Item>
          <Form.Item label='店铺销量' name='sellCount'>
            <Input style={{ height: 40 }}></Input>
          </Form.Item>
          <Form.Item label='活动支持' name='supports'>
            {active.map((item, index) =>
              <Checkbox style={{ marginRight: 15 }} key={index}>{item}</Checkbox>
            )}
            {/* <Checkbox style={{ marginRight: 15 }}>单人精彩套餐</Checkbox>
            <Checkbox>VC无限橙果汁全场8折</Checkbox>
            <Checkbox>在线支付满28减五</Checkbox>
            <Checkbox>特价饮品八折抢购</Checkbox>
            <Checkbox>中秋特惠</Checkbox>
            <Checkbox>国庆特价</Checkbox>
            <Checkbox>春节1折折扣</Checkbox> */}
          </Form.Item>
          <Form.Item label='营业时间' name='date'>
            <TimePicker.RangePicker
              // defaultValue={rangePickerValue}
              format="HH:mm:ss"
            />
          </Form.Item>
        </Form>

        <Upload
          // action="https://660d2bd96ddfa2943b33731c.mockapi.io/api/upload"
          listType="picture-card"
          fileList={fileList}
          onChange={onChange}
          onPreview={onPreview}
        // style={{listStyle:'none'}}
        >
          <PlusOutlined />
        </Upload>









      </Panner >
    </>
  )
}
