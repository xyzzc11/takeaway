import React from 'react'
import { Outlet } from 'react-router-dom'
import LeftMenu from '../../components/LeftMenu'
import Header from '../../components/Header'
import './index.scss'

export default function Layout() {
  return (
    <>
      <LeftMenu></LeftMenu>
      <div className="right">
        <Header></Header>
        <div className="content">
          <Outlet></Outlet>
        </div>
      </div>
    </>
  )
}
