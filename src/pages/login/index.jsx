import React from 'react';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Form, Input, message } from 'antd';
import { useNavigate } from 'react-router-dom';
import { checkLogin } from '../../api/account';
import './index.scss'


const Login = () => {

  const navigate = useNavigate()
  const onFinish = (values) => {
    checkLogin(values).then(res => {
      const { code, msg } = res.data
      if (code == 0) {
        const { id, role, token } = res.data
        localStorage.setItem('user', JSON.stringify({ id, role, token }))
        navigate('/home')
        message.open({
          type: 'success',
          content: msg,
        })
      } else {
        message.open({
          type: 'error',
          content: msg,
        })
      }
    })
  };
  return (
    <div className='login'>
      <div className='login-title'>系统登录</div>
      <Form
        name="normal_login"
        onFinish={onFinish}
        className='login-form'
      >
        <Form.Item
          name="account"
          rules={[
            {
              required: true,
              message: '请输入您的账号!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="请输入账号" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: '请输入您的密码!',
            },
          ]}
        >
          <Input.Password
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="请输入密码"
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            登录
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
export default Login;