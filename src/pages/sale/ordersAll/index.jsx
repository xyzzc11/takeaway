import React, { useEffect } from 'react'
import * as echarts from 'echarts'
import { statsOrder } from '../../../api/report'

export default function CommodityAll() {
  const chart = (data) => {
    // 底部周数
    const week = data.date
    // 图表数据
    const series = []
    // 顶部标题
    const title = []
    data.source.forEach(item => {
      series.push({
        name: item.type,
        type: 'line',
        data: item.data
      })
      title.push(item.type)
    })
    let chart = echarts.init(document.querySelector('.order'))
    const option = {
      title: {
        text: '商品统计'
      },
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: title
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      xAxis: {
        type: 'category',
        boundaryGap: false,
        data: week
      },
      yAxis: {
        type: 'value'
      },
      series: series
    };
    chart.setOption(option);
    const resizeHandler = () => {
      chart.resize();
    };
    window.addEventListener('resize', resizeHandler);
  }

  useEffect(() => {
    statsOrder({}).then(res => {
      chart(res.data.data)
    })
  }, [])

  return (
    <>
      <div className="order" style={{ width: '100%', height: 500 }}></div>
    </>
  )
}
