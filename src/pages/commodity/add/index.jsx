import React, { useEffect, useState } from 'react'
import Panner from '../../../components/Panner';
import {
  Button,
  Form,
  Input,
  InputNumber,
  Select,
  Upload,
  message,
} from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import './index.scss'
import { goodsAdd, goodsCategories, goodsImgUpload } from '../../../api/goods';
const { TextArea } = Input;

export default function Add() {
  const [componentSize, setComponentSize] = useState('default');
  const onFormLayoutChange = ({ size }) => {
    setComponentSize(size);
  };
  // 全部分类
  const [type, setType] = useState([])
  const [name, setName] = useState()
  const [category, setCategory] = useState()
  const [price, setPrice] = useState()
  const [imgUrl, setImgUrl] = useState()
  const [goodsDesc, setGoodsDesc] = useState()
  const allType = async (value) => {
    const res = await goodsCategories(value)
    setType(res.data.categories)
  }
  // 图片 文件上传
  const uploadGoodsImg = async (e) => {
    const formData = new FormData()
    formData.append('file', e.file)
    const res = await goodsImgUpload(formData);
    setImgUrl(res.data.imgUrl.replace('/upload/imgs/goods_img/', ''))
  }
  const add = async (data) => {
    let res = await goodsAdd(data);
    const { code, msg } = res.data
    if (code == 0) {
      message.open({
        type: 'success',
        content: msg,
      })
    } else {
      message.open({
        type: 'error',
        content: msg,
      })
    }
  }
  // 添加商品
  const addGoods = () => {
    const data = {
      name, category, price, imgUrl, goodsDesc
    }
    add(data)
  }
  useEffect(() => {
    allType()
  }, [])
  return (
    <>
      <Panner>
        <div slot='title'>商品添加</div>
        <div className="goods-add" >
          <Form
            labelCol={{
              span: 4,
            }}
            wrapperCol={{
              span: 14,
            }}
            layout="horizontal"
            initialValues={{
              size: componentSize,
            }}
            onValuesChange={onFormLayoutChange}
            size={componentSize}
            style={{
              maxWidth: 600,
            }}
          >
            <Form.Item label="商品名称">
              <Input onChange={(e) => setName(e.target.value)} />
            </Form.Item>
            <Form.Item label="商品分类">
              <Select onChange={(e) => setCategory(e)}>
                {type.map((item, index) => {
                  return < Select.Option key={index} value={item.cateName}>{item.cateName}</Select.Option>
                })}
              </Select>
            </Form.Item>
            <Form.Item label="商品价格">
              <InputNumber min={1} defaultValue={1} onChange={(e) => setPrice(e)} />
            </Form.Item>
            <Form.Item label="商品图片" valuePropName="fileList">
              <Upload customRequest={uploadGoodsImg} listType="picture-card" showUploadList={false} maxCount={1}>
                <div style={{ position: 'relative', width: '102px', height: '102px' }}>
                  <button
                    style={{
                      border: 0,
                      background: 'none',
                      position: 'absolute',
                      top: 0,
                      left: 0,
                      width: '100%',
                      height: '100%',
                    }}
                    type="button"
                  >
                    <PlusOutlined />
                  </button>
                  {imgUrl && <img src={'http://8.137.157.16:9002/upload/imgs/goods_img/' + imgUrl} style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', overflow: 'hidden' }} />}
                </div>
              </Upload>
            </Form.Item>
            <Form.Item label="商品描述">
              <TextArea rows={2} onChange={(e) => setGoodsDesc(e.target.value)} />
            </Form.Item>
            <Form.Item label="" wrapperCol={{ offset: 4 }}>
              <Button type="primary" size={'large'} onClick={addGoods}>添加商品</Button>
            </Form.Item>
          </Form>
        </div>
      </Panner >
    </>
  )
}
