import React from 'react'
import Panner from '../../../components/Panner'
import { Button, Form, Input, message } from 'antd';
import { usersCheckOldPwd, usersEditPwd } from '../../../api/account';
import { useNavigate } from 'react-router-dom';

export default function Update() {
  const layout = {
    labelCol: {
      span: 4,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };
  const [form] = Form.useForm();
  const navigate = useNavigate()
  // const onGenderChange = (value) => {
  // };
  const onFinish = (values) => {
    // 输入的数据
    const { oldPwd, newPwd, confirmPwd } = values
    const id = JSON.parse(localStorage.getItem('user')).id
    // checkOldPwd({ id, oldPwd })
    if (newPwd == confirmPwd) {
      edit({ id, oldPwd, newPwd })
    }
  };
  const onReset = () => {
    form.resetFields();
  };
  // 验证旧密码
  const checkOldPwd = async (value) => {
    let res = await usersCheckOldPwd(value)
    console.log(res);
  }
  // 修改密码
  const edit = async (data) => {
    let res = await usersEditPwd(data)
    const { code, msg } = res.data
    if (code == 0) {
      message.open({
        type: 'success',
        content: msg,
      })
      navigate('/login')
    }
    else {
      message.open({
        type: 'error',
        content: msg,
      })
    }
  }
  return (
    <>
      <Panner>
        <div slot='title'>修改密码</div>
        <Form
          {...layout}
          form={form}
          name="control-hooks"
          onFinish={onFinish}
          style={{
            maxWidth: 600,
            marginTop: 20
          }}
        >
          <Form.Item
            name="oldPwd"
            label="原密码"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input style={{ height: 40 }} />
          </Form.Item>
          <Form.Item
            name="newPwd"
            label="新密码"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input style={{ height: 40 }} />
          </Form.Item>
          <Form.Item
            name="confirmPwd"
            label="确认新密码"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input style={{ height: 40 }} />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit" style={{ height: 40 }}>
              立即修改
            </Button>
          </Form.Item>
        </Form>
      </Panner>

    </>
  )
}
