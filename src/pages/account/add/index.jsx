import React from 'react'
import Panner from '../../../components/Panner'
import { usersAdd } from '../../../api/account';
import { Button, Form, Input, Select, Space, message } from 'antd';
const { Option } = Select

export default function Add() {
  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 6,
      span: 16,
    },
  };
  const [form] = Form.useForm();
  // 请求接口
  const add = async (value) => {
    let res = await usersAdd(value)
    const { code, msg } = res.data
    if (code == 0) {
      message.open({
        type: 'success',
        content: msg,
      })
    } else {
      message.open({
        type: 'error',
        content: msg,
      })
    }
  }
  // 下拉列表的值
  const onGenderChange = (value) => {
  };
  // 添加账号
  const onFinish = (values) => {
    // 输入的数据value
    add(values)
  };
  // 重置
  const onReset = () => {
    form.resetFields();
  };
  return (
    <>
      <Panner>
        <div slot='title'>添加账号</div>
        <Form
          {...layout}
          form={form}
          name="control-hooks"
          onFinish={onFinish}
          style={{
            maxWidth: 400,
            marginTop: 20
          }}
        >
          <Form.Item
            name="account"
            label="账号"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input style={{ height: 40 }} />
          </Form.Item>
          <Form.Item
            name="password"
            label="密码"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Input style={{ height: 40 }} />
          </Form.Item>
          <Form.Item
            name="userGroup"
            label="用户组"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <Select
              placeholder="请选择用户组"
              onChange={onGenderChange}
              allowClear
              style={{ height: 40 }}
            >
              <Option value="超级管理员">超级管理员</Option>
              <Option value="普通管理员">普通管理员</Option>
            </Select>
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Space>
              <Button type="primary" htmlType="submit" style={{ height: 40 }}>
                立即添加
              </Button>
              <Button htmlType="button" onClick={onReset} style={{ height: 40 }}>
                重置
              </Button>
            </Space>
          </Form.Item>
        </Form>
      </Panner>
    </>
  )
}

