import React, { useEffect, useState } from 'react'
import Panner from '../../../components/Panner'
import { List, Upload } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { usersAccountInfo, usersAvatarEdit, usersAvatarUpload } from '../../../api/account'
import dayjs from 'dayjs';
import { useDispatch } from 'react-redux';
import { updateImg } from '../../../store/slice/userSlice';

export default function Center() {
  const [user, setUser] = useState([])
  const [imgUrl, setImgUrl] = useState()
  //拿到可以调用修改方法的派发对象
  const dispatch = useDispatch()
  // localStorage获取id
  const userInfo = JSON.parse(localStorage.getItem('user'))
  const id = userInfo.id
  const accountInfo = async (value) => {
    const res = await usersAccountInfo(value)
    const { id, account, userGroup, ctime, imgUrl } = res.data.accountInfo
    const cTime = dayjs(ctime).format('YYYY-MM-DD')
    setUser([
      {
        title: '管理员ID:',
        content: id
      },
      {
        title: '账号:',
        content: account
      },
      {
        title: '用户组:',
        content: userGroup
      },
      {
        title: '创建时间:',
        content: cTime
      },
      {
        title: '管理员头像:'
      }
    ])
    setImgUrl(imgUrl.replace('/upload/imgs/acc_img/', ''))
  }
  // 上传头像
  const uploadUsersImg = async (e) => {
    const formData = new FormData()
    formData.append('file', e.file)
    const res = await usersAvatarUpload(formData);
    setImgUrl(res.data.imgUrl.replace('/upload/imgs/acc_img/', ''))
    // 通过派发 提交一次修改数据 调用修改数据的方法
    dispatch(updateImg(res.data.imgUrl))
    // const userAvatar = {
    //   id: id,
    //   imgUrl: 'http://8.137.157.16:9002' + res.data.imgUrl
    // }
    // editAvatar({ userAvatar })
  }
  // 修改头像
  // const editAvatar = async (value) => {
  //   const res = await usersAvatarEdit(value)
  //   console.log(res);
  // }
  useEffect(() => {
    accountInfo({ id })
  }, [])
  return (
    <>
      <Panner>
        <div slot='title'>个人中心</div>
        <List
          dataSource={user}
          renderItem={(item) => (
            <List.Item>
              <div style={{ display: 'flex' }}>
                <span style={{ marginRight: 10 }}>{item.title}</span>
                <span>{item.content}</span>
              </div>
            </List.Item>
          )}
          style={{ marginLeft: 15, marginTop: 10 }}
        >
          {/* 用户头像 */}
          <Upload customRequest={uploadUsersImg} listType="picture-card" showUploadList={false} maxCount={1}>
            <div style={{ position: 'relative', width: '102px', height: '102px' }}>
              <button
                style={{
                  border: 0,
                  background: 'none',
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  width: '100%',
                  height: '100%',
                }}
                type="button"
              >
                <PlusOutlined />
              </button>
              {<img src={'http://8.137.157.16:9002/upload/imgs/acc_img/' + imgUrl} style={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%', overflow: 'hidden' }} />}
            </div>
          </Upload>
        </List>
      </Panner>
    </>
  )
}
