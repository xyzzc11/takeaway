import { createHashRouter, Navigate } from "react-router-dom";
import Login from '../pages/login'
import Layout from "../pages/layout";
import Home from '../pages/home'
import Orders from '../pages/orders'
import Shop from '../pages/shop'
import AccountAdd from "../pages/account/add";
import AccountCenter from "../pages/account/center";
import AccountList from '../pages/account/list'
import AccountUpdate from "../pages/account/update";
import CommodityAdd from "../pages/commodity/add";
import CommodityClassFy from "../pages/commodity/classify";
import CommodityList from "../pages/commodity/list";
import CommodityAll from "../pages/sale/commodityAll";
import OrdersAll from "../pages/sale/ordersAll";


const route = [
    {
        path: '/',
        element: <Navigate to='/login'></Navigate>
    },
    {
        path: '/login',
        element: <Login />
    },
    {
        path: '/home',
        loader: () => ({ title: '后台首页', key: '/home' }),
        element: <Layout />,
        children: [
            {
                index: true,
                loader: () => ({ key: '/home' }),
                element: <Home />
            }
        ]
    },
    {
        path: '/account',
        loader: () => ({ title: '账号管理', key: '/account1' }),
        element: <Layout />,
        children: [
            {
                index: true,
                loader: () => ({ title: '账号列表', key: '/account' }),
                element: <AccountList />
            },
            {
                path: '/account/center',
                loader: () => ({ title: '个人中心', key: '/account/center' }),
                element: <AccountCenter />
            },
            {
                path: '/account/add',
                loader: () => ({ title: '添加账号', key: '/account/add' }),
                element: <AccountAdd />
            },
            {
                path: '/account/update',
                loader: () => ({ title: '修改密码', key: '/account/update' }),
                element: <AccountUpdate />
            }
        ]
    },
    {
        path: '/commodity',
        loader: () => ({ title: '商品管理', key: '/commodity1' }),
        element: <Layout />,
        children: [
            {
                index: true,
                loader: () => ({ title: '商品列表', key: '/commodity' }),
                element: <CommodityList />
            },
            {
                path: '/commodity/add',
                loader: () => ({ title: '商品添加', key: '/commodity/add' }),
                element: <CommodityAdd />
            },
            {
                path: '/commodity/classify',
                loader: () => ({ title: '商品分类', key: '/commodity/classify' }),
                element: <CommodityClassFy />
            }
        ]
    },
    {
        path: '/orders',
        loader: () => ({ title: '订单管理', key: '/orders' }),
        element: <Layout />,
        children: [
            {
                index: true,
                loader: () => ({ key: '/orders' }),
                element: <Orders />
            }
        ]
    },
    {
        path: '/sale',
        loader: () => ({ title: '销售统计', key: '/sale1' }),
        element: <Layout />,
        children: [
            {
                index: true,
                loader: () => ({ title: '商品统计', key: '/sale' }),
                element: <CommodityAll />
            },
            {
                path: '/sale/orders',
                loader: () => ({ title: '订单统计', key: '/sale/orders' }),
                element: <OrdersAll />
            }
        ]
    },
    {
        path: '/shop',
        loader: () => ({ title: '店铺管理', key: '/shop' }),
        element: <Layout />,
        children: [
            {
                index: true,
                loader: () => ({ key: '/shop' }),
                element: <Shop />
            }

        ]
    }
]

const router = createHashRouter(route)

export default router