import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
    // 模块名
    name: 'user',
    // 定义数据
    initialState: {
        userImg: ''
    },
    // 操作数据方法
    reducers: {
        // 获取数据并解构使用payload
        updateImg(state, { payload }) {
            state.userImg = payload
        }
    }
})
// 暴露给主模块使用
export default userSlice.reducer
// 暴露修改方法
export const { updateImg } = userSlice.actions