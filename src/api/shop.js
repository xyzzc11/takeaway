import axios from '../utils/http'

// 27. 获取店铺详情
export const shopInfo = () => {
    return axios.get('/shop/info', { params: {} })
}

// 店铺图片--文件上传接口
export const shopUpload = (data) => {
    return axios.post('/shop/upload', data)
}

// 29.店铺内容修改
export const shopEdit = (data) => {
    return axios.post('/shop/edit', data)
}