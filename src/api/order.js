import axios from '../utils/http'

// 24. 获取订单列表(带查询功能)
export const orderList = (data) => {
    return axios.get('/order/list', { params: data })
}

// 25. 获取订单详情
export const orderDetail = (data) => {
    return axios.get('/order/detail', { params: data })
}

// 修改订单
export const orderEdit = (data) => {
    return axios.post('/order/edit', data)
}