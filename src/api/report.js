import axios from '../utils/http'

// 首页报表
export const homeChart = (data) => {
    return axios.get('/stats/total', { params: data })
}

// 31. 订单报表接口
export const statsOrder = (data) => {
    return axios.get('/stats/order', { params: data })
}

// 32. 商品报表接口
export const statsGoods = (data) => {
    return axios.get('/stats/goods', { params: data })
}