import axios from '../utils/http'

// 20. 获取商品列表
export const goodsList = (data) => {
    return axios.get('/goods/list', { params: data })
}

// 17. 查询所有分类名称
export const goodsCategories = (data) => {
    return axios.get('/goods/categories', { params: data })
}

// 19. 添加商品
export const goodsAdd = (data) => {
    return axios.post('/goods/add', data)
}

// 商品图片 文件上传接口
export const goodsImgUpload = (data) => {
    return axios.post('/goods/goods_img_upload', data)
}

// 14. 获取分类
export const goodsCateList = (data) => {
    return axios.get('/goods/catelist', { params: data })
}

// 22. 修改商品
export const goodsEdit = (data) => {
    return axios.post('/goods/edit', data)
}

// 21. 删除商品
export const goodsDel = (data) => {
    return axios.get('/goods/del', { params: data })
}

// 16. 修改分类
export const goodsEditCate = (data) => {
    return axios.post('/goods/editcate', data)
}

// 13. 添加分类
export const goodsAddCate = (data) => {
    return axios.post('/goods/addcate', data)
}

// 15. 删除分类
export const goodsDelCate = (data) => {
    return axios.get('/goods/delcate', {params:data})
}
