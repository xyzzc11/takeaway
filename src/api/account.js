import axios from '../utils/http'

// 登录
export const checkLogin = (data) => {
    return axios.post('/users/checkLogin', data)
}

// 获取账号列表
export const userList = (data) => {
    return axios.get('/users/list', { params: data })
}

// 9. 获取账号（个人中心）信息
export const usersAccountInfo = (data) => {
    return axios.get('/users/accountinfo', { params: data })
}

// 10. 头像上传接口
export const usersAvatarUpload = (data) => {
    return axios.post('/users/avatar_upload', data)
}

// 11. 修改用户头像
export const usersAvatarEdit = (data) => {
    return axios.get('/users/avataredit', { params: data })
}

// 2. 添加账号
export const usersAdd = (data) => {
    return axios.post('/users/add', data)
}

// 7. 检查旧密码是否正确
export const usersCheckOldPwd = (data) => {
    return axios.get('/users/checkoldpwd', { params: data })
}

// 修改密码
export const usersEditPwd = (data) => {
    return axios.post('/users/editpwd', data)
}

// 6. 修改账号
export const usersEdit = (data) => {
    return axios.post('/users/edit', data)
}

// 4. 删除账号
export const usersDel = (data) => {
    return axios.get('/users/del', { params: data })
}



