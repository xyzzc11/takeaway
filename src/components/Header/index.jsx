import React, { useEffect, useState } from 'react'
import { Breadcrumb, Dropdown, Space } from 'antd';
import './index.scss'
import { useNavigate, useMatches } from 'react-router-dom';
import { usersAccountInfo } from '../../api/account';
import { DownOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';

export default function Header() {
  const navigate = useNavigate()
  const list = [
    {
      title: <span onClick={() => toPath('/home')}>首页</span>
    }
  ]
  const matches = useMatches()
  const matches1 = matches.filter(item => item.data && item.data.title)
  matches1.forEach(item => {
    list.push({
      title: <span onClick={() => toPath(item.pathname)}>{item.data.title}</span>
    })
  })
  // 面包屑路由
  const toPath = (path) => {
    navigate(path)
  }
  // 下拉列表
  const items = [
    {
      key: 1,
      label: (
        <div onClick={() => toPath('/account/center')}>
          个人中心
        </div>
      )
    },
    {
      key: 2,
      label: (
        <div onClick={() => toPath('/login')}>
          注销登录
        </div>
      )
    }
  ]
  // 账号名和头像
  const [account, setAccount] = useState('')
  const [img, setImg] = useState()
  const id = JSON.parse(localStorage.getItem('user')).id
  const userInfo = async (value) => {
    let res = await usersAccountInfo(value)
    const user = res.data.accountInfo
    setAccount(user.account)
    setImg(user.imgUrl)
  }
// 获取数据的hooks--react-redux里面的数据并解构
  const { userImg } = useSelector(store => store.userSlice)

  useEffect(() => {
    userInfo({ id })
  }, [])
  return (
    <>
      <div className="header">
        <Breadcrumb items={list} className='header-left' />
        <div className="header-right">
          <div className="text">
            {/* 欢迎
            {account}
            登录 */}
            <Dropdown
              menu={{
                items,
              }}
            >
              <a onClick={(e) => e.preventDefault()}>
                欢迎{account}登录
                <DownOutlined className='icon' />
              </a>
            </Dropdown>
          </div>
          <img src={userImg ? 'http://8.137.157.16:9002' + userImg : 'http://8.137.157.16:9002' + img} alt="" />
        </div>
      </div>
    </>
  )
}
