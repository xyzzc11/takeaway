import React from 'react'
import '../../assets/fonts/iconfont.css'
import './index.scss'

export default function MyIcon(props) {

    let { font, size, color } = props
    size = size || '16px'
    color = color || '#A8BAB0'

    return (
        <>
            <i className={'iconfont ' + font} style={{ fontSize: size, color: color }}></i>
        </>
    )
}
