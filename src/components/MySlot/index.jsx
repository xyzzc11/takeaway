import React from 'react'
import './index.scss'

export default function MySlot(props) {
    const data = React.Children.toArray(props.children)
    const title = data.filter(item => item.props.slot == 'title')
    const btn = data.filter(item => item.props.slot == 'btn')
    return (
        <>
            <div className="content-header">
                <div className="content-header-left">{title}</div>
                <div className="content-header-btn">{btn}</div>
            </div>
        </>
    )
}
