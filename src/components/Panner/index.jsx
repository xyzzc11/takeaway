import React from 'react'
import './index.scss'
import MySlot from '../MySlot'

export default function Panner(props) {

  const data = React.Children.toArray(props.children)
  const content = data.filter(item => !item.props.slot)

  return (
    <>
      <MySlot>
        {data}
      </MySlot>
      {content.map(item => item)}

    </>
  )
}
