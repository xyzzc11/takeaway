import React from 'react'

import { RouterProvider } from 'react-router-dom'

import './index.scss'
import router from './router/router'

export default function App() {
  return (
    <>

      <RouterProvider router={router}></RouterProvider>

    </>
  )
}
